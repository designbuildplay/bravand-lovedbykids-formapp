<!doctype html>
<html lang="en">
<head>
  <title>LOVED BY KIDS</title>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

     <!-- WEBAPP META INFO : APP ICONS -->
    <meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <link rel="apple-touch-icon-precomposed" href="imgs/appico.png"/> 

    <!-- FONTS AND STYLES -->
    <link rel="stylesheet" href="css/kids.css">

    <script type="text/javascript" src="js/lib/jquery-1.7.2.min.js"></script> 
    <script type="text/javascript" src="js/lib/jquery.form.js"></script> 
    <script type="text/javascript" src="js/lib/gs/TweenMax.min.js"></script>
    <script type="text/javascript" src="js/lib/gs/easing/EasePack.min.js"></script>
    <script type="text/javascript" src="js/lib/pixi.dev.js"></script> 
    <script type="text/javascript" src="js/lib/fastclick.js"></script> 
</head>
<body>

<div id="wrapper">

  <!-- PRELOADER -->
  <div id="preloader"></div>

  <!-- STATIC -->
  <div id="spiral"></div>
  
  <div id="welcome">
      <h1 class="txt-shadow-green">TAKE PART IN OUR <br>TEACHERS AND TECH SURVEY!</h1>
      <div class="txt-sub txt-shadow-green">This year, we're asking teachers to give their opinion on key matters surrounding technology <br>in the classroom. Take part in our short survey to share your thoughts and discover what <br>other teachers had to say.</div>
      <div class="logo" id="logoInt"></div>
      <div class="btn-welcome" id="btn-welcome">LET'S GET STARTED</div>
  </div>
  
   <div id="thankyou">
      <h2 class="txt-shadow-green">Your details <br>have been submitted!</h2>
      <div class="txt-sub txt-shadow-green margT margB">Thanks for your interest</div>
      <div class="logo margB"></div>
      <div class="margT "><img src="imgs/share-twit.png" border='0' /></div>
      <div><img src="imgs/share-fb.png" border='0' /></div>
  </div>
  

  <!-- UI ===================================== -->
  <div id="fade"></div>
  <div id="fadeTop"></div>
  <div id="btn-restart"></div>
  <div id="patchCharater"></div>
  <div id="charact1"></div> 
  <div id="charact2"></div>
  <div id="charact3"></div>
  <div id="charact4"></div>
  <div id="tabs">
      <div id="pg1"></div>
      <div id="pg2"></div>
      <div id="pg3"></div>
  </div>
  <div id="btnNext"></div>
  <!-- <div id="btnSubmit"></div> -->

  <div id="quitPop">
    <div class="pop-info">
        <p class="txt-title-ans">ARE YOU SURE?</p>
        <p class="dashedline"></p>
        <p class="txt-red">If you start again now your answers won't be saved. <br>Are you sure you want to start again?</p>
        <p id="quitNo" class="btn-quit-option">NO</p>
        <p id="quitYes" class="btn-quit-option">YES</p>
    </div>
   
  </div>

  <div id="termsPop">
    <div class="pop-info">
        <p class="txt-title-ans">TERMS &amp; CONDITIONS</p>
        <p class="dashedline"></p>
        <div class="scrollarea">
            <p class="">Please read these competition rules carefully. <br>If you enter one of our competitions, we will assume <br>that you have read these rules and that you agree <br>to them.</p>

              <p class="margT">Story Station Subscription Competition <br>Terms and Conditions</p>
              <p>Closing date for entries: 30th March 2014</p>

              <p class="margT">1. To enter the competition you must be: (a) UK resident; and (b) 18 years old or over at the time of entry; (c) working for a UK Primary School.</p>

              <p class="margT">2. No purchase necessary.</p>

              <p class="margT">3. Only one entry per person.</p>

              <p class="margT">4. The Story Station prize winners will be selected at random from participating entrants. We will notify you by email within 48 working hours of the closing date. If you do not respond within 72 hours of being notified, you will forfeit your prize and Loved By Kids reserves the right to choose another winner. The judges’ decision will be final, and no correspondence will be entered into.</p>

              <p class="margT">5. You can find out who has won the competition by sending a stamped addressed envelope, marked with the name of the competition to Loved By Kids Ltd, Marine House, Tide Mill Way, Woodbridge, Suffolk, IP12 1AP after the closing date.</p>

              <p class="margT">6. The prize will not be transferable to another person.</p>

              <p class="margT">7. No part of a prize is exchangeable for cash or any other prize.</p>

              <p class="margT">8. This competition is being run by Loved By Kids Ltd of Marine House, Tide Mill Way, Woodbridge, Suffolk.</p>

              <p class="margT">9. Loved By Kids will endeavour to send prizes within a month of the Story Station launch date (currently March 2014), but cannot guarantee this delivery time.</p>

              <p class="margT">10. The competition is governed by English law.</p>

        </div>
        <p id="btnClose" class="btn-quit-option">CLOSE</p>
    </div>
   
  </div>

  <!-- SCREENS ===================================== -->
  <section id="intro-animation">
    <canvas id="intro" ></canvas>
  </section> 
  
  <!-- QUESTION 1 :::::::::::::::::::::::::::::::::: -->
  <section id="Q1">
    <div class="bgParacute"></div>
    <div class="bgBook">

      <div class="bookL">
        <p class="txt-title">QUESTION 1</p>
        <p class="dashedline"></p>
        <p class="txt-ques">How confident are you at <br>using technology devices in<br> the classroom?</p>
      </div>

      <div class="bookR">
        <p class="btnA" id="q1a"> Very confident</p>
        <p class="dashedline"></p>
        <p class="btnB" id="q1b"> I'm ok</p>
        <p class="dashedline"></p>
        <p class="btnC" id="q1c"> Not confident</p>
      </div>

    </div>
  </section>

  <!-- QUESTION 2 :::::::::::::::::::::::::::::::::: -->
  <section id="Q2">
    <div class="bgParacute"></div>
    <div class="bgBook">

      <div class="bookL">
        <p class="txt-title">QUESTION 2</p>
        <p class="dashedline"></p>
        <p class="txt-ques">What do you think is the biggest hold-back for teachers who want to use new technology in the classroom?</p>
      </div>

      <div class="bookR">
        <p class="btnA" id="q2a"> Budget</p>
        <p class="dashedline"></p>
        <p class="btnB" id="q2b"> Lack of Training /<br> Expertise</p>
        <p class="dashedline"></p>
        <p class="btnC" id="q2c"> Resistance from <br>School Management</p>
      </div>

    </div>
  </section>  

  <!-- QUESTION 3 :::::::::::::::::::::::::::::::::: -->
  <section id="Q3">
    <div class="bgParacute"></div>
    <div class="bgBook">

      <div class="bookL">
        <p class="txt-title">QUESTION 3</p>
        <p class="dashedline"></p>
        <p class="txt-ques">Do you think that technology will play a bigger or smaller role in your classroom in the future?</p>
      </div>

      <div class="bookR">
        <p class="btnA" id="q3a"> Bigger role</p>
        <p class="dashedline"></p>
        <p class="btnB" id="q3b"> Smaller role</p>
        <p class="dashedline"></p>
        <p class="btnC" id="q3c"> About the same</p>
      </div>

    </div>
  </section>  



<!-- ANSWER 1 :::::::::::::::::::::::::::::::::: -->
  <section id="A1">
    <div class="anserText">
      <p class="txt-title-ans">YOU ANSWERED <span id="a1result">#</span></p>
      <p class="dashedline"></p>
      <p class="percentBall">55</div>
      <p class="txt-red answer-info">of teachers asked said they were REASONABLY CONFIDENT at using technology in the classroom.</p>
      <div class="clear"></div>
        <div class="answer-foot">
            <p class="dashedline"></p>
            <p>Story Station has been built with teachers in mind, making it easy for both you and your classroom to use it right from day one!</p>
        </div>
      </div>
  </section>  

<!-- ANSWER 2 :::::::::::::::::::::::::::::::::: -->
  <section id="A2">
    <div class="anserText">
      <p class="txt-title-ans">YOU ANSWERED <span id="a2result">#</span></p>
      <p class="dashedline"></p>
      <p class="percentBall">73</div>
      <p class="txt-red answer-info">of teachers asked listed BUDGET as the biggest hold back for them using new technology in their classroom.</p>
      <div class="clear"></div>
        <div class="answer-foot">
            <p class="dashedline"></p>
            <p>At £400 per year for a site-licence, accessible in all classrooms, for all pupils, Story Station offers great value for your school.</p>
        </div>
      </div>
  </section>  

  <!-- ANSWER 3 :::::::::::::::::::::::::::::::::: -->
  <section id="A3">
    <div class="anserText">
      <p class="txt-title-ans">YOU ANSWERED <span id="a3result">#</span></p>
      <p class="dashedline"></p>
      <p class="percentBall">87</div>
      <p class="txt-red answer-info">of teachers feel that technology will play a bigger role in the classroom of the future.</p>
      <div class="clear"></div>
        <div class="answer-foot">
            <p class="dashedline"></p>
            <p>Why not ask one of our team about how Story Station can help you combine ICT with core teaching activities in your classroom.</p>
        </div>
      </div>
  </section>  


  <!-- FORM ENTRY :::::::::::::::::::::::::::::::: -->
  <section id="formBox">
    <div class="anserText">
      <p class="txt-title-ans">THANK YOU!</p>
      <p class="dashedline"></p>
      <p>If you would like to find out more about Story Station and be in with a chance of winning an author visit to your school, please fill out the form below.</p>
      
      <div class="inputs">
      <form id="form" action="submit.php" method="post">
        <input type="hidden" name="q1input" id="q1input" value="">
          <input type="hidden" name="q2input" id="q2input" value="">
            <input type="hidden" name="q3input" id="q3input" value="">

        <div class="inputimg inputL"><input name="firstName" type="text" onBlur="app.validate()"  class="topcoat-text-input" placeholder="First name" id="name_first"></div>
        <div class="inputimg inputL"><input name="lastName" type="text" onBlur="app.validate()"  class="topcoat-text-input"placeholder="Last name" id="name_last"></div>
        <div class="inputimg inputL"><input name="school" type="text" onBlur="app.validate()"  class="topcoat-text-input" placeholder="School" id="school"></div>
        <div class="inputimg inputL"><input name="email" type="text" onBlur="app.validate()"  class="topcoat-text-input" placeholder="Email" id="email"></div>
        <div id="valid"></div>
        <div class="clear"></div>
      </div>

      <div id="terms"> <input type="checkbox" id="c1" name="cc" /><label for="c1"><span></span></label> I accept the <a href="#" id="termsLink">terms &amp; conditions</a></div>
    </div>
       <input type="submit" value="" id="btnSubmit" disabled>
   </form>
  </section>

</div>

<!-- SCRIPTS -->
<script src="js/main.js"></script>
<script src="js/app/Grafic.js"></script>
<script src="js/app/Canvas.js"></script>
</body>
</html>