
var w, h,
	count = 0;

var CanvasAni = function(){

	// create an new instance of a pixi stage
	stage = new PIXI.Stage();
	canvas = document.getElementById('intro');
	w = $(window).width();
	h = $(window).height();
	renderer = new PIXI.autoDetectRenderer(w, h, canvas, true );

	scp = this;
	this.cloud1 = new Grafic("imgs/cloud1.png")
	this.cloud2 = new Grafic("imgs/cloud2.png")
	this.cloud3 = new Grafic("imgs/cloud3.png")
	this.cloud4 = new Grafic("imgs/cloud4.png")
	this.ship = new Grafic("imgs/airship.png")
	this.book1 = new Grafic("imgs/book1.png")
	this.book2 = new Grafic("imgs/book2.png")
	this.book3 = new Grafic("imgs/book3.png")
	this.book4 = new Grafic("imgs/book4.png")
	this.book5 = new Grafic("imgs/book5.png")
	this.book6 = new Grafic("imgs/book6.png")
	this.firstPlay = true;
	this.speed = 2;
	//Display group containers -----------------------------
 	this.airship = new PIXI.DisplayObjectContainer();
 	this.books = new PIXI.DisplayObjectContainer();
	// w = renderer.width;
	this.cloud1.img.anchor.x = 0.5;
	this.cloud1.img.anchor.y = 0.5;

	this.ship.img.anchor.x = 0.5;
	this.ship.img.anchor.y = 0.5;

	this.book1.img.anchor.x = this.book1.img.anchor.y = 0.5;
	this.book2.img.anchor.x = this.book2.img.anchor.y = 0.5;
	this.book3.img.anchor.x = this.book3.img.anchor.y = 0.5;
	this.book4.img.anchor.x = this.book4.img.anchor.y = 0.5;
	this.book5.img.anchor.x = this.book5.img.anchor.y = 0.5;
	this.book6.img.anchor.x = this.book6.img.anchor.y = 0.5;

	this.book1.img.scale.x = this.book2.img.scale.x = this.book3.img.scale.x = this.book4.img.scale.x = this.book5.img.scale.x = this.book6.img.scale.x = 0
	this.book1.img.scale.y = this.book2.img.scale.y = this.book3.img.scale.y = this.book4.img.scale.y = this.book5.img.scale.y = this.book6.img.scale.y = 0
	this.book1.img.position.y = this.book2.img.position.y = this.book3.img.position.y = this.book4.img.position.y = this.book5.img.position.y = this.book6.img.position.y = 40
}


CanvasAni.prototype.init = function(event){
	
	scp.books.addChild(scp.book1.img)
	scp.books.addChild(scp.book2.img)
	scp.books.addChild(scp.book3.img)
	scp.books.addChild(scp.book4.img)
	scp.books.addChild(scp.book5.img)
	scp.books.addChild(scp.book6.img)
	scp.airship.addChild(scp.ship.img)
	stage.addChild(scp.cloud1.img)
	stage.addChild(scp.cloud3.img)
	stage.addChild(scp.cloud4.img)
	stage.addChild(scp.airship)
	stage.addChild(scp.cloud2.img)
	stage.addChild(scp.cloud2.img)
	stage.addChild(scp.books)

	scp.cloud1.img.position.y = Math.random() * 500
	scp.cloud2.img.position.y = Math.random() * 500
	scp.cloud3.img.position.y = Math.random() * 500
	scp.cloud4.img.position.y = Math.random() * 500

	scp.cloud1.img.position.x = Math.random() * 500
	scp.cloud2.img.position.x = Math.random() * 500
	scp.cloud3.img.position.x = Math.random() * 900
	scp.cloud4.img.position.x = Math.random() * 800

	
	scp.airship.position.x = w + 200
	scp.airship.position.y = h +300
	TweenLite.to(scp.airship.position, 4, { x:w/2, y:h/2,  delay:0.2, onComplete:scp.dropBookys});
	console.log("CANVAS iS ", canvasani )

	if(scp.firstPlay == true){
		requestAnimFrame( canvasani.animate );	
		firstPlay = false
	}
	
	//scp.dropBookys()
}

CanvasAni.prototype.replay = function(event){
	
	TweenLite.set(scp.book1.img.position, { y:40, x:0 });
	TweenLite.set(scp.book1.img.scale, {x:0,y:0});
	TweenLite.set(scp.book2.img.position, { y:40, x:0 });
	TweenLite.set(scp.book2.img.scale, {x:0,y:0});
	TweenLite.set(scp.book3.img.position, { y:40, x:0 });
	TweenLite.set(scp.book3.img.scale, {x:0,y:0});
	TweenLite.set(scp.book4.img.position, { y:40, x:0 });
	TweenLite.set(scp.book4.img.scale, {x:0,y:0});
	TweenLite.set(scp.book5.img.position, { y:40, x:0 });
	TweenLite.set(scp.book5.img.scale, {x:0,y:0});
	TweenLite.set(scp.book6.img.position, { y:40, x:0 });
	TweenLite.set(scp.book6.img.scale, {x:0,y:0});

	scp.airship.position.x = w + 200
	scp.airship.position.y = h + 300
	TweenLite.to(scp.airship.position, 3, { x:w/2,  y:h/2, delay:0.2, onComplete:scp.dropBookys});
}

CanvasAni.prototype.animate = function(event){
	requestAnimFrame( canvasani.animate );
			
	//console.log("tick", scp)
	scp.cloud1.img.position.x += 1
	scp.cloud2.img.position.x += 0.5
	scp.cloud3.img.position.x += 0.1
	scp.cloud4.img.position.x += 0.3

	canvasani.resetCloud(scp.cloud1.img)
	canvasani.resetCloud(scp.cloud2.img)
	canvasani.resetCloud(scp.cloud3.img)
	canvasani.resetCloud(scp.cloud4.img)

	scp.books.position.x = scp.airship.position.x - 40
	scp.books.position.y = scp.airship.position.y + 70 
	count += 0.01;
	//scp.ship.img.position.x +=  (Math.cos(count*4) );
	scp.ship.img.position.y +=  (Math.cos(count *6) );

	// render the stage
	renderer.render(stage);
}

CanvasAni.prototype.resetCloud = function(cloud){
	var c = cloud
	//console.log(c.position.x) 
	if(c.position.x > w + c.width){
		console.log("reset cloud")
		c.position.x = -100
		c.position.y = Math.random() * 500
	}
}

CanvasAni.prototype.dropBookys = function(cloud){
		console.log("books drop")
		TweenLite.to(scp.book1.img.position, 6, { y:h + 500, x:250, ease:Power1.easeIn  });
		TweenLite.fromTo(scp.book1.img.scale, 3, {x:0,y:0}, {x:1,y:1, ease:Back.easeOut });

		TweenLite.to(scp.book2.img.position, 7, { y:h + 500, x:-200, ease:Power1.easeIn  });
		TweenLite.fromTo(scp.book2.img.scale, 3, {x:0,y:0}, {x:1,y:1, ease:Back.easeOut });

		TweenLite.to(scp.book3.img.position, 5, { y:h + 500, x:100, ease:Power1.easeIn, delay:1  });
		TweenLite.fromTo(scp.book3.img.scale, 3, {x:0,y:0}, {x:1.4,y:1.4, ease:Back.easeOut, delay:1 });

		TweenLite.to(scp.book4.img.position, 8, { y:h + 500, x:80, ease:Power1.easeIn  });
		TweenLite.fromTo(scp.book4.img.scale, 3, {x:0,y:0}, {x:1,y:1, ease:Back.easeOut });

		TweenLite.to(scp.book5.img.position, 6, { y:h + 500, x:0, ease:Power1.easeOut, delay:0.5  });
		TweenLite.fromTo(scp.book5.img.scale, 3, {x:0,y:0}, {x:1.4,y:1.4, ease:Back.easeOut, delay:0.5 });

		TweenLite.to(scp.book6.img.position, 5, { y:h + 500, x:-80, ease:Power1.easeOut, delay:2.5  });
		TweenLite.fromTo(scp.book6.img.scale, 3, {x:0,y:0}, {x:1,y:1, ease:Back.easeOut, delay:2.5 });

		TweenLite.to(scp.airship.position, 4, { y:-300, delay:2, ease:Power1.easeOut});
}


CanvasAni.prototype.resize = function(){
		w = $(window).width();
		h = $(window).height();
		
		renderer.resize(w, h);
		// scp.airship.position.y = h/2;
		// TweenLite.to(scp.airship.position, 3, { x:w/2  });
		console.log("d")
}


CanvasAni.prototype.yo = function(){
		//console.log("yes lad")
		canvasani.init()
			
}

var canvasani = new CanvasAni()
$(window).resize(canvasani.resize);
